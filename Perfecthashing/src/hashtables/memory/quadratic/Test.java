package hashtables.memory.quadratic;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import hashtables.memory.linear.LinearHashTable;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File fileName = new File ("keys10001000.txt");
		Scanner input = null;
         try {
			 input = new Scanner(fileName);
		} catch (FileNotFoundException e) {
			System.out.println(e);
			e.printStackTrace();
		}
         
         String keys = input.nextLine();
        String[] keysN =keys.split(",");
        
        Set<Integer> keyNum = new HashSet<Integer> ();
        for (String zew : keysN) {
        	keyNum.add(Integer.parseInt(zew.trim()));
        }
        Set<Integer> test = new HashSet<Integer> (2);
        test.add(3);
        test.add(3);
        HashTable hash = new HashTable(100,keyNum);
        //LinearHashTable hashLinear = new LinearHashTable(100,keyNum);
        for (int i =0;i<1000;i++) {
        	 HashTable hashLinear = new HashTable(1000,keyNum);
            System.out.print(i+" "+hashLinear.getRehashingCount());
            System.out.println(" "+hashLinear.size());
        }
        
        
	}

}
