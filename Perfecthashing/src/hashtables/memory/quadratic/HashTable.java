package hashtables.memory.quadratic;

import java.util.ArrayList;

import java.util.Set;

import hashtables.HashFunction;
import hashtables.HashFunctionGenerator;

public class HashTable {

	private ArrayList<Integer> hashTable;
	private HashFunctionGenerator hashGenerator;
	private HashFunction hashFunc;
	private int tableSize;
	private int rehashingCount = 0;

	public HashTable(int keyRange, Set<Integer> keysSet) {

		tableSize = (keysSet.size() * keysSet.size());
		hashGenerator = new HashFunctionGenerator(keyRange, tableSize);
		hashTable = new ArrayList<>();
		resethashTable();
		buildHashTable(keysSet);

	}

	public void buildHashTable(Set<Integer> keysSet) {

		while (true) {
			hashFunc = hashGenerator.getHashFunction();
			if (testHashFunction(hashFunc, keysSet)) {
				break;
			}
			resethashTable();
			rehashingCount++;
		}

	}

	public boolean testHashFunction(HashFunction hash, Set<Integer> keysSet) {

		for (Integer key : keysSet) {
			int index = hash.getIndex(key);
			if (hashTable.get(index) == -1) {
				hashTable.set(index, key);
			} else {
				return false;
			}
		}
		return true;
	}

	private void resethashTable() {
		hashTable.clear();
		for (int i = 0; i < tableSize; i++) {
			hashTable.add(-1);
		}
	}

	public ArrayList<Integer> getHashTable() {
		return hashTable;
	}

	public int getRehashingCount() {
		return rehashingCount;
	}
	
    public int size() {
	  return hashTable.size(); 
    }
    
	public int search(int key) {
		int index = hashFunc.getIndex(key);
		return hashTable.get(index);
	}
}
