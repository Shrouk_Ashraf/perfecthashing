package hashtables.memory.linear;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import hashtables.HashFunction;
import hashtables.HashFunctionGenerator;
import hashtables.memory.quadratic.HashTable;

public class LinearHashTable {

	private ArrayList<Set<Integer>> levelOneTable;
	private ArrayList<HashTable> hashTable;
	private HashFunctionGenerator hashGenerator;
	private HashFunction hash;
	private int totalSize;
	private int rehashingCount;

	public LinearHashTable(int keyRange, Set<Integer> keysSet) {

		hashGenerator = new HashFunctionGenerator(keyRange, keysSet.size());
		hash = hashGenerator.getHashFunction();
		levelOneTable = new ArrayList<>();
		hashTable = new ArrayList<>();
		rehashingCount = 0;
		totalSize = keysSet.size();
		
        while (true){
		performLevelOneHashing(keysSet);
		performLevelTwoHashing(keyRange);
		 if (totalSize <(5*keysSet.size())) {
			 break;
		 }
		 levelOneTable.clear();
		 hashTable.clear();
		 hash = hashGenerator.getHashFunction();
		 totalSize = keysSet.size();
		 rehashingCount++;
        }
	}

	private void performLevelOneHashing(Set<Integer> keysSet) {
		initializeLevelOne(keysSet.size());
		for (Integer key : keysSet) {
			int index = hash.getIndex(key);
			levelOneTable.get(index).add(key);
		}
	}

	private void performLevelTwoHashing(int keyRange) {
		for (Set<Integer> row : levelOneTable) {
			HashTable quadHashTable = new HashTable(keyRange, row);
			hashTable.add(quadHashTable);
			totalSize += quadHashTable.size();
		}

	}

	public int search(int key) {
		int index = hash.getIndex(key);
		int item = hashTable.get(index).search(key);
		return item;
	}

	private void initializeLevelOne(int Nsize) {
		for (int i = 0; i < Nsize; i++) {
			levelOneTable.add(new HashSet<Integer>());
		}
	}
	
	public int size(){
		return totalSize;
	}
	
	public int rehashingCount(){
	  return rehashingCount;
	}
}
