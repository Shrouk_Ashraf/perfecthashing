package hashtables;

import java.math.BigInteger;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class HashFunctionGenerator {

	private int p,m;
	public HashFunctionGenerator(int keyRange, int tableSize) {
		
		long max = Math.max(keyRange, tableSize);
		BigInteger maxBigInt = new BigInteger(String.valueOf(max));
		p = (int) Long.parseLong(maxBigInt.nextProbablePrime().toString());
		//System.out.println("ppp"+p);
        m = tableSize;
	}
	
	public HashFunction getHashFunction(){
		
		int a = ThreadLocalRandom.current().nextInt(1,p);
		int b = ThreadLocalRandom.current().nextInt(0,p);
		
		return new HashFunction(p,m,a,b);
	  }
	
}
